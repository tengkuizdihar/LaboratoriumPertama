import time
from datetime import date
from django.shortcuts import render

# Enter your name here
mhs_name = 'Tengku Izdihar Rahman Amanullah' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(1999)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today = date.today()
    return today.year - birth_year
